podman-start:
	# podman run \
	# 	--name cradle_postgres \
	# 	-v ./external/bernard/docker/data/psql:/var/lib/postgresql/data \
	# 	-e POSTGRES_PASSWORD=test \
	# 	-p 5432:5432 \
	# 	-d \
	# 	postgres:alpine

	podman run \
		--name cradle_postgres \
		-v ../bernard/docker/data/psql:/var/lib/postgresql/data \
		-e POSTGRES_PASSWORD=test \
		-p 5432:5432 \
		-d \
		postgres:alpine

podman-stop:
	podman container stop cradle_postgres
	podman container rm cradle_postgres

run:
	cargo run --release

build:
	cargo build --release