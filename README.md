# creditspreads
This is a public snapshot of my credit spread trading repositories. The purpose is to build create a system that can be used to test different credit spread trading strategies. Then a bot using these strategies can be hooked up to a live data feed to make trades. 

## Layout
### Cradle
The main repository, which contains the backtesting functionality. It feeds data into a bot like `armistice` to test trading performance.

### `external/armistice`
This repository contains code for managing trades. Currently includes logic for Vertical Put spreads. In the process of adding Vertical Call spreads, Iron Condors, etc.

### `external/teddy`
Rust code for interacting with a PostgreSQL database using diesel.

### `external/bernard`
An application to import zip archives of historical options quotes into a database using Teddy.


### `nb`
This folder contain Jupyter notebooks I've used for analysis.