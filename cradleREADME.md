# Cradle

This is a program to perform options backtests using Rust. It's named after the cradle where all the host are stored and learn how to function. Hopefully and it help teach a trading bot how to function.

## Backtest Types

`cradle` integrates with Armistice to provide backtesting simulation for a bot.

## Configuration

## Directories

### `src`

This is the `src` directory of the Rust project.

### `nb`

This includes Jupyter Notebooks for data analysis.

### `output`

This includes output csv files and logs.

## Design

The `cradle` pipeline for running a backtest is as follows.

1. Get all chains from Teddy for the underlying (sort these by day).
2. For each day with chains.
    1. Open new positions
    2. Manage existing positions
    3. Write results of close positions and update list of open positions
3. Collect the results.

### Traits

The trait `Trade` represents the necessary operations to manage a trade.
This trait is implemented by structs that need to be backtested.

## License

Copyright Parker Damon 2020 All Rights Reserved
