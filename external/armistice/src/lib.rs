use bigdecimal::BigDecimal;

pub mod trade;
pub mod option;
pub mod verticalput;
// pub mod put;

mod testutils;

/// Converts a f64 to a BigDecimal with 3 decimal places of precision.
/// This should be adequate for currency values.
/// 
/// # Examples
/// ```
/// # use armistice::float64_to_bigdec;
/// # use std::str::FromStr;
/// # use bigdecimal::BigDecimal;
/// assert_eq!(BigDecimal::from(1), float64_to_bigdec(1.0));
/// assert_eq!(BigDecimal::from_str("0.5").unwrap(), float64_to_bigdec(0.5));
/// assert_eq!(BigDecimal::from_str("-0.127").unwrap(), float64_to_bigdec(-0.127));
/// ```
pub fn float64_to_bigdec(val: f64) -> BigDecimal {
    let i = (val*1000.0) as i64;
    let x = BigDecimal::from(i);
    return x/1000;
}

/// Converts a f32 to a BigDecimal with 3 decimal places of precision.
/// This should be adequate for currency values.
/// 
/// # Examples
/// ```
/// # use armistice::float32_to_bigdec;
/// # use std::str::FromStr;
/// # use bigdecimal::BigDecimal;
/// 
/// assert_eq!(BigDecimal::from(1), float32_to_bigdec(1.0));
/// assert_eq!(BigDecimal::from_str("0.5").unwrap(), float32_to_bigdec(0.5));
/// assert_eq!(BigDecimal::from_str("-0.127").unwrap(), float32_to_bigdec(-0.127));
/// ```
pub fn float32_to_bigdec(val: f32) -> BigDecimal {
    let i = (val*1000.0) as i64;
    let x = BigDecimal::from(i);
    return x/1000;
}
