use crate::float64_to_bigdec;

use std::collections::HashMap;

use bigdecimal::BigDecimal;
use chrono::NaiveDate;

/// Maps Expirations Dates to a Map of Strikes to Quotes
/// Contains one map for calls and one for puts
#[derive(Debug)]
pub struct Chain {
    calls: HashMap<NaiveDate, HashMap<BigDecimal, Quote>>,
    puts: HashMap<NaiveDate, HashMap<BigDecimal, Quote>>,
}

/// Enum for side of an option trade. Call and Put
#[derive(PartialEq)]
pub enum Side {
    Call,
    Put,
}

/// A struct the represent an option quote.
#[derive(Debug, Clone, PartialEq)]
pub struct Quote {
    bid: BigDecimal,
    ask: BigDecimal,
    last: BigDecimal,
    ns_delta: f32,
    delta: f32,
    gamma: f32,
    theta: f32,
    vega: f32,
    iv: f32,
}

impl Quote {
    /// Function to construct new option quotes
    /// Params: mid_dist represents the distance between the bid and ask to buy
    /// where 1 is buy at the ask and sell at the bid, and 0 is buy at the bid and sell
    /// at the ask. 0.5 would represent all sales occuring at the mid-price.
    pub fn new(bid: f64, ask: f64, last: f64, ns_delta: f32, delta: f32, gamma: f32, theta: f32, vega: f32, iv: f32) -> Quote {

        Quote {
            ask: float64_to_bigdec(ask),
            bid: float64_to_bigdec(bid),
            last: float64_to_bigdec(last),
            ns_delta: ns_delta,
            delta: delta,
            gamma: gamma,
            theta: theta,
            vega: vega,
            iv: iv,
        }
    }

    /// Function to construct new option quotes using Big Decimal type quotes.
    /// Params: mid_dist represents the distance between the bid and ask to buy
    /// where 1 is buy at the ask and sell at the bid, and 0 is buy at the bid and sell
    /// at the ask. 0.5 would represent all sales occuring at the mid-price.
    pub fn newbd(bid: BigDecimal, ask:BigDecimal, last:BigDecimal, ns_delta: f32, delta: f32, gamma: f32, theta: f32, vega: f32, iv: f32) -> Quote {
        Quote {
            ask: ask,
            bid: bid,
            last: last,
            ns_delta: ns_delta,
            delta: delta,
            gamma: gamma,
            theta: theta,
            vega: vega,
            iv: iv,
        }
    }

    pub fn bid(&self) -> BigDecimal {
        self.bid.clone()
    }

    pub fn ask(&self) -> BigDecimal {
        self.ask.clone()
    }

    pub fn last(&self) -> BigDecimal {
        self.last.clone()
    }

    pub fn delta(&self) -> f32 {
        self.delta.clone()
    }

    pub fn gamma(&self) -> f32 {
        self.gamma.clone()
    }

    pub fn theta(&self) -> f32 {
        self.theta.clone()
    }

    pub fn vega(&self) -> f32 {
        self.vega.clone()
    }

    pub fn iv(&self) -> f32 {
        self.iv.clone()
    }
}

impl Chain {
    /// Inserts an option quote into an option chain.
    /// 
    /// # Examples
    /// ```
    /// # use chrono::NaiveDate;
    /// # use armistice::option::*;
    /// 
    /// let q = Quote::new(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
    /// let mut chain = Chain::new();
    /// 
    /// chain.insert_quote(q, NaiveDate::from_ymd(2021, 1, 1), 10.0, Side::Put);
    /// ```
    pub fn insert_quote(&mut self, q: Quote, exp: NaiveDate, strike: f64, side: Side) {
        match side {
            Side::Call => {
                match self.calls.get_mut(&exp) {
                    Some(m) => {
                        m.insert(float64_to_bigdec(strike), q);
                    },
                    None => {
                        let mut m = HashMap::<BigDecimal, Quote>::new();
                        m.insert(float64_to_bigdec(strike), q);
                        self.calls.insert(exp, m);
                    }
                }
            }
            Side::Put => {
                match self.puts.get_mut(&exp) {
                    Some(m) => {
                        m.insert(float64_to_bigdec(strike), q);
                    },
                    None => {
                        let mut m = HashMap::<BigDecimal, Quote>::new();
                        m.insert(float64_to_bigdec(strike), q);
                        self.puts.insert(exp, m);
                    }
                }
            }
        }
    }

    /// Gets a reference to the calls side of the chain.
    pub fn get_calls(&self) -> &HashMap<NaiveDate, HashMap<BigDecimal, Quote>> {
        &self.calls
    }

    /// Gets a reference to the puts side of the chain.
    pub fn get_puts(&self) -> &HashMap<NaiveDate, HashMap<BigDecimal, Quote>> {
        &self.puts
    }

    /// Constructions a new option chain.
    pub fn new() -> Chain {
        Chain {
            calls: HashMap::<NaiveDate, HashMap<BigDecimal, Quote>>::new(),
            puts: HashMap::<NaiveDate, HashMap<BigDecimal, Quote>>::new(),
        }
    }
}