use crate::{float64_to_bigdec, option::Chain, trade::{Trade, TradeError, MakeParams, ManageParams, ManageResult}};

use bigdecimal::BigDecimal;
use chrono::NaiveDate;


#[derive(Debug, PartialEq, Eq, Hash)]
pub struct ShortPut {
    strike: BigDecimal,
    exp_date: NaiveDate,
    open_date: Option<NaiveDate>,
    open_credit: Option<BigDecimal>,
    close_date: Option<NaiveDate>,
    close_debit: Option<BigDecimal>,
}


impl Trade for ShortPut {
    fn make(chain: &Chain, _p: &MakeParams) -> Vec<ShortPut> {
        let mut v = Vec::new();
        for (exp, c) in chain.get_puts() {
            for (s, _) in c {
                let sp = ShortPut {
                    strike: s.clone(),
                    exp_date: exp.clone(),
                    open_date: None,
                    open_credit: None,
                    close_date: None,
                    close_debit: None,
                };

                v.push(sp);
            }
        }

        v.sort_by(|a, b| a.strike.cmp(&b.strike));

        return v;
    }

    fn open(&mut self, md: f64, date: NaiveDate, chain: &Chain) -> Result<(), TradeError> {
        let mdbd = float64_to_bigdec(md);
        let q = match chain.get_puts().get(&self.exp_date) {
            Some(em) => {
                match em.get(&self.strike) {
                    Some(q) => q,
                    None => return Err(TradeError::ChainMissingStrike)
                }
            },
            None => return Err(TradeError::ChainMissingExpiration)
        };

        self.open_date = Some(date);
        self.open_credit = Some(q.bid() + (q.ask() - q.bid())*mdbd);

        return Ok(());
    }

    fn is_open(&self) -> bool {
        match self.open_credit {
            Some(_) => match self.close_debit {
                Some(_) => false,
                None => true,
            },
            None => false
        }
    }

    /// TODO finish this
    fn manage(&mut self, _chain: &Chain, _p: &ManageParams) -> Result<ManageResult, TradeError> {
        Ok(ManageResult::Nothing)
    }

    fn write(&self) -> String {
        let mut s = String::new();

        s += &self.strike.to_string();
        s += ", ";

        s += &self.exp_date.to_string();
        s += ", ";

        s += &self.open_date.unwrap().to_string();
        s += ", ";

        s += &self.open_credit.as_ref().unwrap().to_string();
        s += ", ";

        s += &self.close_date.unwrap().to_string();
        s += ", ";

        s += &self.close_debit.as_ref().unwrap().to_string();

        s
    }

    fn write_header() -> String {
        "strike, expiration, open_date, open_credit, close_date, close_debit".to_string()
    }
}

#[derive(Debug, PartialEq, Eq, Hash)]
pub struct LongPut {
    strike: BigDecimal,
    exp_date: NaiveDate,
    open_date: Option<NaiveDate>,
    open_debit: Option<BigDecimal>,
    close_date: Option<NaiveDate>,
    close_credit: Option<BigDecimal>,
}


impl Trade for LongPut {
    fn make(chain: &Chain, _p: &MakeParams) -> Vec<LongPut> {
        let mut v = Vec::new();
        for (exp, c) in chain.get_puts() {
            for (s, _) in c {
                let sp = LongPut {
                    strike: s.clone(),
                    exp_date: exp.clone(),
                    open_date: None,
                    open_debit: None,
                    close_date: None,
                    close_credit: None,
                };

                v.push(sp);
            }
        }

        return v;
    }

    fn open(&mut self, md: f64, date: NaiveDate, chain: &Chain) -> Result<(), TradeError> {
        let mdbd = float64_to_bigdec(md);
        let q = match chain.get_puts().get(&self.exp_date) {
            Some(em) => {
                match em.get(&self.strike) {
                    Some(q) => q,
                    None => return Err(TradeError::ChainMissingStrike)
                }
            },
            None => return Err(TradeError::ChainMissingExpiration)
        };

        self.open_date = Some(date);
        self.open_debit = Some(q.ask() - (q.ask() - q.bid())*mdbd);

        return Ok(());
    }

    fn is_open(&self) -> bool {
        match self.open_debit {
            Some(_) => match self.close_credit {
                Some(_) => false,
                None => true,
            },
            None => false
        }
    }

    /// TODO finish this
    fn manage(&mut self, _chain: &Chain, _p: &ManageParams) -> Result<ManageResult, TradeError> {
        Ok(ManageResult::Nothing)
    }

    fn write(&self) -> String {
        let mut s = String::new();

        s += &self.strike.to_string();
        s += ", ";

        s += &self.exp_date.to_string();
        s += ", ";

        s += &self.open_date.unwrap().to_string();
        s += ", ";

        s += &self.open_debit.as_ref().unwrap().to_string();
        s += ", ";

        s += &self.close_date.unwrap().to_string();
        s += ", ";

        s += &self.close_credit.as_ref().unwrap().to_string();

        s
    }

    fn write_header() -> String {
        "strike, expiration, open_date, open_credit, close_date, close_debit".to_string()
    }

    fn update_stats(&mut self) {
        
    }
}

#[cfg(test)]
mod test_put {
    use super::*;
    use crate::testutils::make_test_chain;

    #[test]
    fn test_make_shortput() {
        let chain = make_test_chain();

        let puts = ShortPut::make(&chain, &MakeParams::new(BigDecimal::from(0)));

        let p1 = ShortPut {
            strike: BigDecimal::from(9),
            exp_date: NaiveDate::from_ymd(2021, 3, 19),
            open_date: None,
            open_credit: None,
            close_date: None,
            close_debit: None,
        };

        let p2 = ShortPut {
            strike: BigDecimal::from(10),
            exp_date: NaiveDate::from_ymd(2021, 3, 19),
            open_date: None,
            open_credit: None,
            close_date: None,
            close_debit: None,
        };

        assert_eq!(p1, puts[0]);
        assert_eq!(p2, puts[1]);
    }
}