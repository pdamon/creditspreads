use crate::option::{Chain, Quote, Side};

use chrono::NaiveDate;

#[allow(dead_code)]
pub fn make_test_chain() -> Chain {
    let mut chain = Chain::new();    

    chain.insert_quote(q1(), exp1(), 10.0, Side::Put);
    chain.insert_quote(q2(), exp1(), 9.0, Side::Put);
    chain.insert_quote(q3(), exp1(), 8.0, Side::Put);
    chain.insert_quote(q4(), exp1(), 7.0, Side::Put);
    chain.insert_quote(q5(), exp1(), 6.0, Side::Put);
    chain.insert_quote(q6(), exp1(), 5.0, Side::Put);
    chain.insert_quote(q1(), exp1(), 10.0, Side::Call);
    chain.insert_quote(q2(), exp1(), 11.0, Side::Call);
    chain.insert_quote(q3(), exp1(), 12.0, Side::Call);
    chain.insert_quote(q4(), exp1(), 13.0, Side::Call);
    chain.insert_quote(q5(), exp1(), 14.0, Side::Call);
    chain.insert_quote(q6(), exp1(), 15.0, Side::Call);

    chain.insert_quote(q1(), exp2(), 10.0, Side::Put);
    chain.insert_quote(q2(), exp2(), 9.0, Side::Put);
    chain.insert_quote(q3(), exp2(), 8.0, Side::Put);
    chain.insert_quote(q4(), exp2(), 7.0, Side::Put);
    chain.insert_quote(q5(), exp2(), 6.0, Side::Put);
    chain.insert_quote(q6(), exp2(), 5.0, Side::Put);
    chain.insert_quote(q1(), exp2(), 10.0, Side::Call);
    chain.insert_quote(q2(), exp2(), 11.0, Side::Call);
    chain.insert_quote(q3(), exp2(), 12.0, Side::Call);
    chain.insert_quote(q4(), exp2(), 13.0, Side::Call);
    chain.insert_quote(q5(), exp2(), 14.0, Side::Call);
    chain.insert_quote(q6(), exp2(), 15.0, Side::Call);

    chain
}

#[allow(dead_code)]
pub fn q1() -> Quote {
    Quote::new(1.0, 1.1, 1.05, 0.35, 0.35, 0.1, 0.1, 0.01, 0.25)
}

#[allow(dead_code)]
pub fn q2() -> Quote {
    Quote::new(0.75, 0.8, 0.85, 0.25, 0.25, 0.05, 0.05, 0.01, 0.28)
}

#[allow(dead_code)]
pub fn q3() -> Quote {
    Quote::new(0.66, 0.69, 0.69, 0.2, 0.2, 0.03, 0.04, 0.01, 0.25)
}

#[allow(dead_code)]
pub fn q4() -> Quote {
    Quote::new(0.49, 0.53, 0.50, 0.1, 0.1, 0.02, 0.02, 0.01, 0.28)
}

#[allow(dead_code)]
pub fn q5() -> Quote {
    Quote::new(0.30, 0.38, 0.35, 0.05, 0.05, 0.01, 0.1, 0.01, 0.25)
}

#[allow(dead_code)]
pub fn q6() -> Quote {
    Quote::new(0.15, 0.23, 0.20, 0.02, 0.03, 0.01, 0.05, 0.01, 0.30)
}

#[allow(dead_code)]
pub fn exp1() -> NaiveDate {
    NaiveDate::from_ymd(2021, 3, 19)
}

pub fn exp2() -> NaiveDate {
    NaiveDate::from_ymd(2021, 4, 16)
}