use crate::{float64_to_bigdec, option::{Chain, Quote}};

use std::{fmt, hash::Hash};

use bigdecimal::BigDecimal;
use chrono::NaiveDate;

/// Enum type for the result of managing a trade.
#[derive(Debug, PartialEq)]
pub enum ManageResult {
    Nothing,
    Close,
    Assigned,
    // Roll,
}

/// Enum type for errors in executing/managing a trade.
#[derive(Debug, PartialEq)]
pub enum TradeError {
    SpreadNotClosed,
    SpreadNotOpen,
    ChainMissingExpiration,
    ChainMissingStrike,
}

impl fmt::Display for TradeError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &TradeError::SpreadNotClosed => write!(f, "TradeError: Spread Not Closed!"),
            &TradeError::SpreadNotOpen => write!(f, "TradeError: Spread Not Open!"),
            &TradeError::ChainMissingExpiration => write!(f, "TradeError: Chain Missing Expiration!"),
            &TradeError::ChainMissingStrike => write!(f, "TradeError: Chain Missing Strike!"),
        }
    }
}

/// Struct that holds parameters for managing a credit spread.
pub struct ManageParams {
    profit_target: BigDecimal,
    current_date: NaiveDate,
    mid_dist: BigDecimal,
}

impl ManageParams {
    /// Creates a new ManageParams struct.
    /// 
    /// ### Arguments
    /// * pt: profit target
    /// * md: mid distance 1 is perfect fill, 0 is worst fill, 0.5 is fill at mid
    /// * cd: The current date.
    pub fn new(pt: f64, md: f64, cd: NaiveDate) -> ManageParams {
        ManageParams {
            profit_target: float64_to_bigdec(pt),
            current_date: cd,
            mid_dist: float64_to_bigdec(md),
        }
    }

    /// Gets the profit target.
    pub fn get_pt(&self) -> BigDecimal {
        self.profit_target.clone()
    }

    /// Gets the current date.
    pub fn get_cd(&self) -> NaiveDate {
        self.current_date.clone()
    }

    /// Gets the mid distance.
    pub fn get_md(&self) -> BigDecimal {
        self.mid_dist.clone()
    }
}

/// Holds parameters for trade::make(). Current just holds spread width.
pub struct MakeParams {
    width: BigDecimal
}

impl MakeParams {
    /// Creates a new MakeParams struct.
    pub fn new(width: BigDecimal) -> MakeParams {
        MakeParams {
            width: width
        }
    }

    /// Gets the spread width.
    pub fn get_width(&self) -> &BigDecimal {
        &self.width
    }
}

/// Contains statistics on a trade. Currently holds low and high price values.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Stats {
    low: BigDecimal,
    high: BigDecimal,
}

impl Stats {
    /// Returns a new Stats struct.
    pub fn new() -> Stats {
        Stats {
            low: BigDecimal::from(999999999),
            high: BigDecimal::from(0),
        }
    }

    /// Gets the low value.
    pub fn get_low(&self) -> BigDecimal {
        self.low.clone()
    }

    /// Gets the high value.
    pub fn get_high(&self) -> BigDecimal {
        self.high.clone()
    }

    /// Updates the low value. Ignores new value if it's greater than
    /// the already stored value.
    /// 
    /// # Examples
    /// ```
    /// # use armistice::trade::Stats;
    /// # use bigdecimal::BigDecimal;
    /// 
    /// let mut s = Stats::new();
    ///
    /// s.update_low(BigDecimal::from(5));
    /// s.update_low(BigDecimal::from(7));
    /// s.update_low(BigDecimal::from(4));

    /// assert_eq!(BigDecimal::from(4), s.get_low());
    /// ```
    pub fn update_low(&mut self, current: BigDecimal) {
        if current < self.low {
            self.low = current
        }
    }

    /// Updates the high value. Ignores new value if it's less than
    /// the already stored value.
    /// 
    /// # Examples
    /// ```
    /// # use armistice::trade::Stats;
    /// # use bigdecimal::BigDecimal;
    /// 
    /// let mut s = Stats::new();
    ///
    /// s.update_high(BigDecimal::from(5));
    /// s.update_high(BigDecimal::from(4));
    /// s.update_high(BigDecimal::from(7));

    /// assert_eq!(BigDecimal::from(7), s.get_high());
    /// ```
    pub fn update_high(&mut self, current: BigDecimal) {
        if current > self.high {
            self.high = current
        }
    }
}

/// A trait used to implement the different types of credit spread trade.
pub trait Trade: Eq + Hash {
    /// Makes a list of all possible trades from an option chain and the given parameters.
    fn make(chain: &Chain, p: &MakeParams) -> Vec::<Self>
    where Self: std::marker::Sized ;

    /// Manages a trade. 
    fn manage(&mut self, chain: &Chain, p: &ManageParams) -> Result<ManageResult, TradeError>;

    /// Opens the current spread based upon the option chain.
    fn open(&mut self, md: f64, date: NaiveDate, chain: &Chain) -> Result<(), TradeError>;

    /// Checks if the spread is currently open.
    fn is_open(&self) -> bool;

    /// Gets open price of the trade. Returns None if the spread hasn't been opened.
    fn get_open_price(&self) -> Option<BigDecimal>;

    /// Gets the closing price if the spread is closed. Otherwise returns None.
    fn get_close_price(&self) -> Option<BigDecimal>;

    /// Gets a current quote given the option chain.
    fn get_quote(&self, chain: &Chain) -> Result<Quote, TradeError>;


    /// Updates the trade's statistics.
    fn update_stats(&mut self, chain: &Chain);

    /// Writes the results from a spread to a file.
    fn write(&self) -> Vec::<String>;

    /// Writes the corresponding header columns for an output CSV file.
    fn write_header() -> Vec::<String>;
}
