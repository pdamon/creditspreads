use crate::trade::{Trade, TradeError, MakeParams, ManageParams, ManageResult};
use crate::option::{Quote, Chain};
use crate::float64_to_bigdec;

use bigdecimal::BigDecimal;
use chrono::NaiveDate;


/// A struct that represents a Vertical put spread trade.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct SVPut {
    exp_date: NaiveDate,
    short_strike: BigDecimal,
    long_strike: BigDecimal,
    open_credit: Option<BigDecimal>,
    close_debit: Option<BigDecimal>,
    open_date: Option<NaiveDate>,
    close_date: Option<NaiveDate>,
}

impl SVPut {
    pub fn new(exp: NaiveDate, ss: BigDecimal, ls: BigDecimal) -> SVPut {
        SVPut {
            exp_date: exp,
            short_strike: ss,
            long_strike: ls,
            open_credit: None,
            close_debit: None,
            open_date: None,
            close_date: None,
        }
    }
}

impl Trade for SVPut {
    fn make(chain: &Chain, p: &MakeParams) -> Vec::<SVPut> {
        let mut spreads = Vec::<SVPut>::new();

        for (exp, exp_chain) in chain.get_puts() {
            for (strike, _) in exp_chain {
                match exp_chain.get(&(strike + p.get_width())) {
                    Some(_) => {
                        spreads.push(SVPut::new(exp.clone(), strike.clone(), strike.clone() + p.get_width()));
                    },
                    None => {}
                }
            }
        }

        spreads.sort_by(|a, b| a.short_strike.cmp(&b.short_strike));
        
        return spreads;
    }

    fn open(&mut self, md: f64, date: NaiveDate, chain: &Chain) -> Result<(), TradeError> {
        let e = match chain.get_puts().get(&self.exp_date) {
            Some(m) => m,
            None => return Err(TradeError::ChainMissingExpiration)
        };

        let sq = match e.get(&self.short_strike) {
            Some(q) => q,
            None => return Err(TradeError::ChainMissingStrike)
        }; 

        let lq = match e.get(&self.long_strike) {
            Some(q) => q,
            None => return Err(TradeError::ChainMissingStrike)
        };

        let b = sq.bid() - lq.ask();
        let a = sq.ask() - lq.bid();
        
        self.open_credit = Some(&b + (&a - &b)*float64_to_bigdec(md));
        self.open_date = Some(date);

        Ok(())
    }

    fn is_open(&self) -> bool {
        match self.open_credit {
            Some(_) => true,
            None => false
        }
    }

    // fn get_open_credit(&self) -> Option<BigDecimal> {
    //     self.open_credit.clone()
    // }

    fn get_profit(&self) -> Option<BigDecimal> {
        match &self.open_credit {
            Some(op) => {
                match &self.close_debit {
                    Some(cl) => Some(op - cl),
                    None => None
                }
            },
            None => None
        }
    }

    fn get_quote(&self, chain: &Chain) -> Result<Quote, TradeError> {
        let ec = match chain.get_puts().get(&self.exp_date) {
            Some(m) => {
                m
            },
            None => return Err(TradeError::ChainMissingExpiration)
        };

        let sq = match ec.get(&self.short_strike) {
            Some(q) => q,
            None => return Err(TradeError::ChainMissingStrike)
        };

        let lq = match ec.get(&self.long_strike) {
            Some(q) => q,
            None => return Err(TradeError::ChainMissingStrike)
        };

        Ok(Quote::short_vertical_quote(sq, lq))
    }

    fn manage(&mut self, chain: &Chain, p: &ManageParams) -> Result<ManageResult, TradeError> {
        let q = self.get_quote(chain)?;
        let oc = self.open_credit.clone().ok_or(TradeError::SpreadNotOpen)?;

        if &p.get_cd() > &self.exp_date {
            return Err(TradeError::SpreadNotOpen);
        }

        // close for a profit
        let bbp = q.ask() - (q.bid() - q.ask())*p.get_md(); // the buy back price

        if bbp <= (BigDecimal::from(1) - p.get_pt())*oc {
            self.close_debit = Some(bbp);
            self.close_date = Some(p.get_cd());

            return Ok(ManageResult::Close);
        }

        // get assigned
        if self.exp_date == p.get_cd() {
            self.close_debit = Some(&self.short_strike - &self.long_strike);
            self.close_date = Some(self.exp_date);

            return Ok(ManageResult::Assigned);
        }


        // roll the spread
        
        Ok(ManageResult::Nothing)
    }

    fn write_header() -> String {
        "open_date, open_credit, close_date, close_debit, open_delta, open_theta, open_vega".to_string()
    }

    fn write(&self, open_chain: &Chain, _close_chain: &Chain) -> String {
        let mut s = String::new();
        let oq = self.get_quote(open_chain).unwrap();

        s += &(self.open_date.unwrap().to_string() + ", ");
        s += &(self.open_credit.as_ref().unwrap().to_string() + ", ");
        s += &(self.close_date.unwrap().to_string() + ", ");
        s += &(self.close_debit.as_ref().unwrap().to_string() + ", ");
        s += &(oq.delta().to_string() + ", ");
        s += &(oq.theta().to_string() + ", ");
        s += &(oq.vega().to_string());

        s
    }
}


#[cfg(test)]
mod test_svputs {
    use super::*;
    use std::str::FromStr;

    #[test]
    fn test_make() {
        let q = Quote::new(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

        let mut chain = Chain::new();
        let e = NaiveDate::from_ymd(2021, 1, 1);

        for i in vec![1, 2, 3, 4, 5, 6, 7, 8] {
            chain.insert_quote(q.clone(), e.clone(), i as f64, crate::option::Side::Put);
        }

        let p = MakeParams::new(BigDecimal::from(5));
        let spreads = SVPut::make(&chain, &p);

        let v1 = SVPut::new(e.clone(), BigDecimal::from(1), BigDecimal::from(6));
        let v2 = SVPut::new(e.clone(), BigDecimal::from(2), BigDecimal::from(7));
        let v3 = SVPut::new(e.clone(), BigDecimal::from(3), BigDecimal::from(8));

        assert_eq!(3, spreads.len());        
        assert_eq!(v1, spreads[0]);
        assert_eq!(v2, spreads[1]);
        assert_eq!(v3, spreads[2]);
    }

    #[test]
    fn test_open() {
        let mut v = SVPut::new(NaiveDate::from_ymd(2021, 1, 31), BigDecimal::from(10), BigDecimal::from(7));

        let sq = Quote::new(4.0, 5.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
        let lq = Quote::new(3.0, 4.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
        let mut chain = Chain::new();
        
        chain.insert_quote(sq, NaiveDate::from_ymd(2021, 1, 31), 10.0, crate::option::Side::Put);
        chain.insert_quote(lq, NaiveDate::from_ymd(2021, 1, 31), 7.0, crate::option::Side::Put);

        assert_eq!(false, v.is_open());

        v.open(0.75, NaiveDate::from_ymd(2021, 1, 1), &chain).unwrap();

        let eq = Quote::new(0.0, 2.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
        assert_eq!(true, v.is_open());
        assert_eq!(eq, v.get_quote(&chain).unwrap());
        assert_eq!(BigDecimal::from_str("1.5").unwrap(), v.open_credit.unwrap());
    }

    #[test]
    fn test_open_errors() {
        let mut v = SVPut::new(NaiveDate::from_ymd(2021, 1, 31), BigDecimal::from(10), BigDecimal::from(7));

        let q = Quote::new(4.0, 5.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
        let mut chain = Chain::new();

        // missing expiration
        assert_eq!(Err(TradeError::ChainMissingExpiration), v.open(0.75, NaiveDate::from_ymd(2021, 1, 1), &chain));
        
        // missing short strike
        chain.insert_quote(q.clone(), NaiveDate::from_ymd(2021, 1, 31), 12.0, crate::option::Side::Put);
        assert_eq!(Err(TradeError::ChainMissingStrike), v.open(0.75, NaiveDate::from_ymd(2021, 1, 1), &chain));

        // missing long strike
        chain.insert_quote(q, NaiveDate::from_ymd(2021, 1, 31), 10.0, crate::option::Side::Put);
        assert_eq!(Err(TradeError::ChainMissingStrike), v.open(0.75, NaiveDate::from_ymd(2021, 1, 1), &chain));
    }

    #[test]
    fn test_manage() {
        let mut v = SVPut::new(NaiveDate::from_ymd(2021, 1, 31), BigDecimal::from(10), BigDecimal::from(7));

        let open_sq = Quote::new(4.0, 5.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
        let open_lq = Quote::new(3.0, 4.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
        let mut open_chain = Chain::new();
        
        open_chain.insert_quote(open_sq, NaiveDate::from_ymd(2021, 1, 31), 10.0, crate::option::Side::Put);
        open_chain.insert_quote(open_lq, NaiveDate::from_ymd(2021, 1, 31), 7.0, crate::option::Side::Put);
        v.open(0.75, NaiveDate::from_ymd(2021, 1, 1), &open_chain).unwrap();

        let close_sq = Quote::new(3.25, 3.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
        let close_lq = Quote::new(2.5, 2.75, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
        let mut close_chain = Chain::new();
        
        close_chain.insert_quote(close_sq, NaiveDate::from_ymd(2021, 1, 31), 10.0, crate::option::Side::Put);
        close_chain.insert_quote(close_lq, NaiveDate::from_ymd(2021, 1, 31), 7.0, crate::option::Side::Put);
        
        let p = ManageParams::new(0.25, 0.25, NaiveDate::from_ymd(2021, 1, 15));
        let r = v.manage(&close_chain, &p);

        assert_eq!(ManageResult::Close, r.unwrap());
        assert_eq!(BigDecimal::from_str("0.375").unwrap(), v.get_profit().unwrap());
    }

    #[test]
    fn test_manage_assignment() {
        let mut v = SVPut::new(NaiveDate::from_ymd(2021, 1, 31), BigDecimal::from(10), BigDecimal::from(7));

        let open_sq = Quote::new(4.0, 5.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
        let open_lq = Quote::new(3.0, 4.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
        let mut open_chain = Chain::new();
        
        open_chain.insert_quote(open_sq, NaiveDate::from_ymd(2021, 1, 31), 10.0, crate::option::Side::Put);
        open_chain.insert_quote(open_lq, NaiveDate::from_ymd(2021, 1, 31), 7.0, crate::option::Side::Put);
        v.open(0.75, NaiveDate::from_ymd(2021, 1, 1), &open_chain).unwrap();

        let close_sq = Quote::new(6.0, 7.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
        let close_lq = Quote::new(1.0, 1.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
        let mut close_chain = Chain::new();
        
        close_chain.insert_quote(close_sq, NaiveDate::from_ymd(2021, 1, 31), 10.0, crate::option::Side::Put);
        close_chain.insert_quote(close_lq, NaiveDate::from_ymd(2021, 1, 31), 7.0, crate::option::Side::Put);
        
        let p = ManageParams::new(0.25, 0.25, NaiveDate::from_ymd(2021, 1, 31));
        let r = v.manage(&close_chain, &p);

        assert_eq!(ManageResult::Assigned, r.unwrap());
        assert_eq!(BigDecimal::from_str("-1.5").unwrap(), v.get_profit().unwrap());
    }
}