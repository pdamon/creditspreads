use crate::trade::{Trade, TradeError, MakeParams, ManageParams, ManageResult, Stats};
use crate::option::{Quote, Chain};
use crate::float64_to_bigdec;

use bigdecimal::BigDecimal;
use chrono::NaiveDate;


/// A struct that represents a Vertical put spread trade.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct VerticalPut {
    exp_date: NaiveDate,
    short_strike: BigDecimal,
    long_strike: BigDecimal,
    open_price: Option<BigDecimal>,
    close_price: Option<BigDecimal>,
    open_date: Option<NaiveDate>,
    close_date: Option<NaiveDate>,
    stats: Stats,
}

impl VerticalPut {
    /// Constructs a new Vertical Put struct.
    pub fn new(exp: NaiveDate, ss: BigDecimal, ls: BigDecimal) -> VerticalPut {
        VerticalPut {
            exp_date: exp,
            short_strike: ss,
            long_strike: ls,
            open_price: None,
            close_price: None,
            open_date: None,
            close_date: None,
            stats: Stats::new(),
        }
    }

    /// Takes a short leg quote and long leg quote to make a quote for the vertical spread.
    fn make_quote(sq: &Quote, lq: &Quote) -> Quote {
        Quote::newbd(
            &sq.bid() - &lq.ask(),
            &sq.ask() - &lq.bid(),
            &sq.last() - &lq.last(),
            sq.delta(),
            &sq.delta() - &lq.delta(),
            &sq.gamma() - &lq.gamma(),
            &sq.theta() - &lq.theta(),
            &sq.vega() - &lq.vega(),
            (&sq.iv() + &lq.iv())/2.0,
        )
    }
}

impl Trade for VerticalPut {
    /// Makes a list of all Vertical Put spreads that can be made using the given
    /// chain and given parameters.
    fn make(chain: &Chain, p: &MakeParams) -> Vec::<VerticalPut> {
        let mut spreads = Vec::<VerticalPut>::new();

        for (exp, exp_chain) in chain.get_puts() {
            for (strike, _) in exp_chain {
                match exp_chain.get(&(strike + p.get_width())) {
                    Some(_) => {
                        spreads.push(VerticalPut::new(exp.clone(), strike + p.get_width(), strike.clone()));
                    },
                    None => {}
                }
            }
        }

        spreads.sort_by(|a, b| a.short_strike.cmp(&b.short_strike));
        
        return spreads;
    }

    /// Opens the spread based upon the given chain, date, and mid-distance.
    /// md = 0 is worst fill. md = 1 is best fill. md = 0.5 is fill at mid.
    fn open(&mut self, md: f64, date: NaiveDate, chain: &Chain) -> Result<(), TradeError> {
        let e = match chain.get_puts().get(&self.exp_date) {
            Some(m) => m,
            None => return Err(TradeError::ChainMissingExpiration)
        };

        let sq = match e.get(&self.short_strike) {
            Some(q) => q,
            None => return Err(TradeError::ChainMissingStrike)
        }; 

        let lq = match e.get(&self.long_strike) {
            Some(q) => q,
            None => return Err(TradeError::ChainMissingStrike)
        };

        let b = sq.bid() - lq.ask();
        let a = sq.ask() - lq.bid();
        
        self.open_price = Some(&b + (&a - &b)*float64_to_bigdec(md));
        self.open_date = Some(date);

        Ok(())
    }

    /// Returns true if the spread is currently open. Otherwise false.
    fn is_open(&self) -> bool {
        match self.open_price {
            Some(_) => match self.close_price {
                Some(_) => false,
                None => true,
            },
            None => false
        }
    }

    /// Getter for the opening price of the spread.
    fn get_open_price(&self) -> Option<BigDecimal> {
        self.open_price.clone()
    }

    fn get_close_price(&self) -> Option<BigDecimal> {
        match &self.close_price {
            Some(cl) => Some(cl.clone()),
            None => None
        }
    }

    /// Gets a quote for the spread base upon the given chain.
    fn get_quote(&self, chain: &Chain) -> Result<Quote, TradeError> {
        let ec = match chain.get_puts().get(&self.exp_date) {
            Some(m) => {
                m
            },
            None => return Err(TradeError::ChainMissingExpiration)
        };

        let sq = match ec.get(&self.short_strike) {
            Some(q) => q,
            None => return Err(TradeError::ChainMissingStrike)
        };

        let lq = match ec.get(&self.long_strike) {
            Some(q) => q,
            None => return Err(TradeError::ChainMissingStrike)
        };

        Ok(VerticalPut::make_quote(sq, lq))
    }

    /// Manages the spread.
    /// 
    /// Closes the spread if the given profit target has been reached.
    /// 
    /// If given parameter date is the expiration date then the spread is closed
    /// at the calculated fill price (base on the mid-distance) or at the width 
    /// of the strikes.
    /// 
    /// TODO: Implement rolling spreads.
    fn manage(&mut self, chain: &Chain, p: &ManageParams) -> Result<ManageResult, TradeError> {
        // check if past the expiration date, this matters for assignment of quarterly options
        if p.get_cd() > self.exp_date {
            self.close_price = Some(&self.short_strike - &self.long_strike);
            self.close_date = Some(p.get_cd());
            return Ok(ManageResult::Assigned);
        }

        let q = self.get_quote(chain)?;
        let oc = self.open_price.clone().ok_or(TradeError::SpreadNotOpen)?;

        // close for a profit
        let bbp = q.bid() + (q.ask() - q.bid())*(BigDecimal::from(1) - p.get_md()); // the buy back price

        if bbp <= (BigDecimal::from(1) - p.get_pt())*oc {
            self.close_price = Some(bbp);
            self.close_date = Some(p.get_cd());

            return Ok(ManageResult::Close);
        }

        // get assigned
        if p.get_cd() >= self.exp_date {
            if bbp > &self.short_strike - &self.long_strike {
                self.close_price = Some(&self.short_strike - &self.long_strike);
            } else {
                self.close_price = Some(bbp);
            }

            self.close_date = Some(p.get_cd());

            return Ok(ManageResult::Assigned);
        }

        // TODO: roll the spread
        
        Ok(ManageResult::Nothing)
    }

    /// Updates the stats for the spread based on the given chain.
    fn update_stats(&mut self, chain: &Chain) {
        match self.get_quote(chain) {
            Ok(q) => {
                self.stats.update_low((q.bid() + q.ask())/2);
                self.stats.update_high((q.bid() + q.ask())/2);
            },
            Err(_) => {}
        }
    }

    /// Outputs the results of the spread as a list of strings. This can be passed to a
    /// csv::Writer object.
    /// 
    /// # Panics
    /// Panics of the trade hasn't been opened and then closed.
    fn write(&self) -> Vec::<String> {
        let mut s = Vec::<String>::new();

        s.push(self.short_strike.to_string());
        s.push(self.long_strike.to_string());
        s.push(self.exp_date.to_string());
        s.push(self.open_date.unwrap().to_string());
        s.push(self.open_price.as_ref().unwrap().to_string());
        s.push(self.close_date.unwrap().to_string());
        s.push(self.close_price.as_ref().unwrap().to_string());

        s
    }

    /// Creates a list of strings that make up the header fields of a CSV file that
    /// contains the results from vertical put simulations.
    /// 
    /// # Examples
    /// ```
    /// # use armistice::{trade::Trade, verticalput::VerticalPut};
    /// let v = vec!["short_strike".to_string(),
    ///         "long_strike".to_string(),
    ///         "expiration".to_string(),
    ///         "open_date".to_string(), 
    ///         "open_price".to_string(), 
    ///         "close_date".to_string(), 
    ///        "close_price".to_string()];
    ///
    /// assert_eq!(v, VerticalPut::write_header());
    /// ```
    fn write_header() -> Vec::<String> {
        vec!["short_strike".to_string(),
             "long_strike".to_string(),
             "expiration".to_string(),
             "open_date".to_string(), 
             "open_price".to_string(), 
             "close_date".to_string(), 
             "close_price".to_string()]
    }
}


#[cfg(test)]
mod test_verticalputs {
    use super::*;
    use crate::testutils::*;
    use std::str::FromStr;

    #[test]
    fn test_make() {
        let chain = make_test_chain();

        let p = MakeParams::new(BigDecimal::from(3));
        let spreads = VerticalPut::make(&chain, &p);

        let v1 = VerticalPut::new(exp1(), BigDecimal::from(8), BigDecimal::from(5));
        let v2 = VerticalPut::new(exp1(), BigDecimal::from(9), BigDecimal::from(6));
        let v3 = VerticalPut::new(exp1(), BigDecimal::from(10), BigDecimal::from(7));

        let v4 = VerticalPut::new(exp2(), BigDecimal::from(8), BigDecimal::from(5));
        let v5 = VerticalPut::new(exp2(), BigDecimal::from(9), BigDecimal::from(6));
        let v6 = VerticalPut::new(exp2(), BigDecimal::from(10), BigDecimal::from(7));

        assert_eq!(6, spreads.len());        
        assert_eq!(v1, spreads[0]);
        assert_eq!(v4, spreads[1]);
        assert_eq!(v2, spreads[2]);
        assert_eq!(v5, spreads[3]);
        assert_eq!(v3, spreads[4]);
        assert_eq!(v6, spreads[5]);
    }

    #[test]
    fn test_make_quote() {
        let nq = VerticalPut::make_quote(&q1(), &q4());
        let gq = Quote::new(0.47, 0.61, 0.55, 0.35, 0.25, 0.08, 0.08, 0.0, 0.265);

        assert_eq!(gq, nq);
    }

    #[test]
    fn test_open() {
        let chain = make_test_chain();
        let mut v = VerticalPut::new(exp1(), BigDecimal::from(10), BigDecimal::from(7));

        assert_eq!(false, v.is_open());

        v.open(0.75, NaiveDate::from_ymd(2021, 1, 1), &chain).unwrap();
        let eq = Quote::new(0.47, 0.61, 0.55, 0.35, 0.25, 0.08, 0.08, 0.0, 0.265);

        assert_eq!(true, v.is_open());
        assert_eq!(eq, v.get_quote(&chain).unwrap());
        assert_eq!(BigDecimal::from_str("0.575").unwrap(), v.get_open_price().unwrap());
        assert_eq!(NaiveDate::from_ymd(2021, 1, 1), v.open_date.unwrap());
    }

    #[test]
    fn test_open_errors() {
        let chain = make_test_chain();

        // missing expiration
        let mut v1 = VerticalPut::new(NaiveDate::from_ymd(2021, 1, 31), BigDecimal::from(10), BigDecimal::from(7));
        assert_eq!(Err(TradeError::ChainMissingExpiration), v1.open(0.75, NaiveDate::from_ymd(2021, 1, 1), &chain));
        
        // missing short strike
        let mut v2 = VerticalPut::new(exp1(), BigDecimal::from(15), BigDecimal::from(10));
        assert_eq!(Err(TradeError::ChainMissingStrike), v2.open(0.75, NaiveDate::from_ymd(2021, 1, 1), &chain));

        // missing long strike
        let mut v3 = VerticalPut::new(exp1(), BigDecimal::from(8), BigDecimal::from(3));
        assert_eq!(Err(TradeError::ChainMissingStrike), v3.open(0.75, NaiveDate::from_ymd(2021, 1, 1), &chain));
    }

    #[test]
    fn test_manage_nothing() {
        let mut v = VerticalPut::new(exp1(), BigDecimal::from(10), BigDecimal::from(7));
        let chain = make_test_chain();

        v.open(0.75, NaiveDate::from_ymd(2021, 1, 1), &chain).unwrap();
        
        let p = ManageParams::new(0.25, 0.25, NaiveDate::from_ymd(2021, 1, 15));
        let r = v.manage(&chain, &p);

        assert_eq!(ManageResult::Nothing, r.unwrap());
        assert_eq!(true, v.is_open());
    }

    #[test]
    fn test_manage_close() {
        let mut v = VerticalPut::new(exp1(), BigDecimal::from(10), BigDecimal::from(7));
        let open_chain = make_test_chain();

        let qs = Quote::new(0.3, 0.35, 0.32, 0.25, 0.0, 0.0, 0.0, 0.0, 0.0);
        let ql = Quote::new(0.05, 0.1, 0.07, 0.05, 0.0, 0.0, 0.0, 0.0, 0.0);
        let mut close_chain = Chain::new();
        close_chain.insert_quote(qs, exp1(), 10.0, crate::option::Side::Put);
        close_chain.insert_quote(ql, exp1(), 7.0, crate::option::Side::Put);

        v.open(0.75, NaiveDate::from_ymd(2021, 1, 1), &open_chain).unwrap();
        
        let p = ManageParams::new(0.25, 0.25, NaiveDate::from_ymd(2021, 2, 1));
        let r = v.manage(&close_chain, &p);

        assert_eq!(ManageResult::Close, r.unwrap());
        assert_eq!(false, v.is_open());
        assert_eq!(BigDecimal::from_str("0.275").unwrap(), v.get_close_price().unwrap());
    }

    #[test]
    fn test_manage_assign_close() {
        let mut v = VerticalPut::new(exp1(), BigDecimal::from(10), BigDecimal::from(7));
        let open_chain = make_test_chain();

        let qs = Quote::new(3.9, 4.0, 0.32, 0.25, 0.0, 0.0, 0.0, 0.0, 0.0);
        let ql = Quote::new(1.0, 1.05, 0.07, 0.05, 0.0, 0.0, 0.0, 0.0, 0.0);
        let mut close_chain = Chain::new();
        close_chain.insert_quote(qs, exp1(), 10.0, crate::option::Side::Put);
        close_chain.insert_quote(ql, exp1(), 7.0, crate::option::Side::Put);

        v.open(0.75, NaiveDate::from_ymd(2021, 1, 1), &open_chain).unwrap();
        
        let p = ManageParams::new(0.25, 0.25, exp1());
        let r = v.manage(&close_chain, &p);

        assert_eq!(ManageResult::Assigned, r.unwrap());
        assert_eq!(false, v.is_open());
        assert_eq!(BigDecimal::from_str("2.9625").unwrap(), v.get_close_price().unwrap());
    }

    #[test]
    fn test_manage_assign_assign() {
        let mut v = VerticalPut::new(exp1(), BigDecimal::from(10), BigDecimal::from(7));
        let open_chain = make_test_chain();

        let qs = Quote::new(4.0, 4.2, 0.32, 0.25, 0.0, 0.0, 0.0, 0.0, 0.0);
        let ql = Quote::new(0.95, 1.0, 0.07, 0.05, 0.0, 0.0, 0.0, 0.0, 0.0);
        let mut close_chain = Chain::new();
        close_chain.insert_quote(qs, exp1(), 10.0, crate::option::Side::Put);
        close_chain.insert_quote(ql, exp1(), 7.0, crate::option::Side::Put);

        v.open(0.75, NaiveDate::from_ymd(2021, 1, 1), &open_chain).unwrap();
        
        let p = ManageParams::new(0.25, 0.25, exp1());
        let r = v.manage(&close_chain, &p);

        assert_eq!(ManageResult::Assigned, r.unwrap());
        assert_eq!(false, v.is_open());
        assert_eq!(BigDecimal::from(3), v.get_close_price().unwrap());
    }

    #[test]
    fn test_write() {
        let mut v = VerticalPut::new(exp1(), BigDecimal::from(10), BigDecimal::from(7));
        let open_chain = make_test_chain();

        let qs = Quote::new(0.3, 0.35, 0.32, 0.25, 0.0, 0.0, 0.0, 0.0, 0.0);
        let ql = Quote::new(0.05, 0.1, 0.07, 0.05, 0.0, 0.0, 0.0, 0.0, 0.0);
        let mut close_chain = Chain::new();
        close_chain.insert_quote(qs, exp1(), 10.0, crate::option::Side::Put);
        close_chain.insert_quote(ql, exp1(), 7.0, crate::option::Side::Put);

        v.open(0.75, NaiveDate::from_ymd(2021, 1, 1), &open_chain).unwrap();
        
        let p = ManageParams::new(0.25, 0.25, NaiveDate::from_ymd(2021, 2, 1));
        let _ = v.manage(&close_chain, &p);

        let w = vec![
            BigDecimal::from(10).to_string(), // short strike
            BigDecimal::from(7).to_string(), // long strike
            exp1().to_string(), // expiration date
            NaiveDate::from_ymd(2021, 1, 1).to_string(), // open date
            BigDecimal::from_str("0.5750").unwrap().to_string(), // open price
            NaiveDate::from_ymd(2021, 2, 1).to_string(), // close date
            BigDecimal::from_str("0.2750").unwrap().to_string(), // close price
        ];

        assert_eq!(w, v.write());
    }

    #[test]
    fn test_update_stats() {
        let mut v = VerticalPut::new(exp1(), BigDecimal::from(10), BigDecimal::from(7));
        let chain = make_test_chain();

        v.open(0.5, NaiveDate::from_ymd(2021, 1, 15), &chain).unwrap();

        v.update_stats(&chain);

        assert_eq!(BigDecimal::from_str("0.540").unwrap(), v.stats.get_high());
        assert_eq!(BigDecimal::from_str("0.540").unwrap(), v.stats.get_low());
    }
}
