# Bernard
I'm naming my trading tools after Westworld characters. It also an executable that allows for all the Delta Neutral zip files to be loaded into the database using Teddy.

## Package Layout

* `bernhard::dn` includes functionality for interacting with Delta Neutral zip files as downloaded.

## License
Copyright Parker Damon 2020 All Rights Reserved
