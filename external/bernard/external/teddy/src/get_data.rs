use super::{schema::*, models::*};
use std::error::Error;

use diesel::prelude::*;

pub fn get_tickers(con: &PgConnection) -> Result<Vec::<EquityTicker>, Box<dyn Error>> {
    let tickers = equity_tickers::table.load(con)?;
    Ok(tickers)
}

pub fn get_exprations(con: &PgConnection) -> Result<Vec::<Expiration>, Box<dyn Error>> {
    let tickers = expirations::table.load(con)?;
    Ok(tickers)
}

pub fn get_options_eod(con: &PgConnection) -> Result<Vec::<OptionQuote>, Box<dyn Error>> {
    let tickers = option_eod::table.load(con)?;
    Ok(tickers)
}

pub fn get_equity_eod(con: &PgConnection) -> Result<Vec::<EquityQuote>, Box<dyn Error>> {
    let tickers = equity_eod::table.load(con)?;
    Ok(tickers)
}

pub fn get_option_symbols(con: &PgConnection) -> Result<Vec::<OptionSymbol>, Box<dyn Error>> {
    let tickers = option_symbols::table.load(con)?;
    Ok(tickers)
}