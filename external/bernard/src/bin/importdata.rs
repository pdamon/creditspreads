use bernard::dn::{OptionRecord, OptionStatsRecord, StockRecord, WatchlistRecord};
use teddy::models::*;

use std::{collections::{HashMap, HashSet}, error::Error, fs::File, sync::mpsc::channel};

use core::panic;
use chrono::NaiveDate;
use glob::glob;
use pbr::ProgressBar;
use threadpool::ThreadPool;
use zip::read::ZipArchive;

type StockMap = HashMap::<String,HashMap<NaiveDate, EquityQuote>>;
type OptionSymbolMap = HashMap::<String, OptionSymbol>;
type Data = (StockMap, HashSet::<NaiveDate>, OptionSymbolMap, Vec::<OptionQuote>);

/// Takes a StockRecord and inserts it into the StockMap.
fn handle_stock_record(r: StockRecord, stocks: &mut StockMap, watchlist: &HashSet<String>) {
    match watchlist.get(&r.symbol) {
        Some(_) => {
            // create the quote
            let q = EquityQuote {
                ticker: r.symbol.clone(),
                date: r.quotedate,
                close: Some(r.close),
                adj_close: Some(r.adjustedclose),
                volume: Some(r.volume),
                iv30call: None,
                iv30put: None,
                iv30mean: None,
            };
            
            match stocks.get_mut(&r.symbol) {
                Some(m) => {
                    // update an existing quote
                    match m.get_mut(&r.quotedate) {
                        Some(d) => {
                            d.ticker = r.symbol;
                            d.date = r.quotedate;
                            d.close = Some(r.close);
                            d.adj_close = Some(r.adjustedclose);
                            d.volume = Some(r.volume);
                        },
                        // insert a new quote
                        None => {
                            m.insert(r.quotedate, q);
                        }
                    }
                },
                // add a new ticker to the map
                None => {
                    let mut m = HashMap::new();
                    m.insert(r.quotedate, q);
                    stocks.insert(r.symbol, m);
                }
            };
        },
        None => {} // the ticker was not in the watchlist so skip and do nothing
    }
}

/// Takes an OptionStatsRecord and inserts it into the StockMap.
fn handle_option_stats(r: OptionStatsRecord, stocks: &mut StockMap, watchlist: &HashSet<String>) {
    match watchlist.get(&r.symbol) {
        Some(_) => {
            // create the quote
            let q = EquityQuote {
                ticker: r.symbol.clone(),
                date: r.quotedate,
                close: None,
                adj_close: None,
                volume: None,
                iv30call: Some(r.iv30call),
                iv30put: Some(r.iv30put),
                iv30mean: Some(r.iv30mean),
            };
            
            match stocks.get_mut(&r.symbol) {
                Some(m) => {
                    // update the quote if already exists
                    match m.get_mut(&r.quotedate) {
                        Some(d) => {
                            d.ticker = r.symbol;
                            d.date = r.quotedate;
                            d.iv30call = Some(r.iv30call);
                            d.iv30put = Some(r.iv30put);
                            d.iv30mean = Some(r.iv30mean);
                        },
                        None => {
                            m.insert(r.quotedate, q); // insert the quote if none already there
                        }
                    }
                },
                // insert a new symbol along with a quote
                None => {
                    let mut m = HashMap::new();
                    m.insert(r.quotedate, q);
                    stocks.insert(r.symbol, m);
                }
            };
        },
        None => {} // ticker not in watchlist, do nothing
    }
}

/// Inserts and OptionRecord in to the results vector. Also updates the options symbol map and the set of expirations.
fn handle_options(r: OptionRecord, options: &mut OptionSymbolMap, expirations: &mut HashSet::<NaiveDate>, watchlist: &HashSet<String>, results: &mut Vec::<OptionQuote>) {
    match watchlist.get(&r.underlying_symbol) {
        Some(_) => {
            // update map of option symbols
            let mut o = OptionSymbol {
                expiration: r.expiration.clone(),
                occ_symbol: r.occ_symbol.clone(),
                strike: r.strike,
                underlying: r.underlying_symbol.clone(),
                is_call: false
            };

            if r.side == "call" {
                o.is_call = true;
            }

            options.insert(r.occ_symbol.clone(), o);

            // update set of expirations
            expirations.insert(r.expiration);

            let q = OptionQuote {
                occ_symbol: r.occ_symbol,
                date: r.quotedate,
                // dte: r.dte,
                bid: Some(r.bid),
                ask: Some(r.ask),
                last: Some(r.last),
                volume: Some(r.volume),
                open_interest: Some(r.open_interest),
                iv: Some(r.iv),
                delta: Some(r.delta),
                gamma: Some(r.gamma),
                theta: Some(r.theta),
                vega: Some(r.vega),
            };

            results.push(q);
        },
        None => {}
    }
}

/// insert_data will read data and then insert it into the database
fn insert_data(data: Data) -> Result<(), Box<dyn Error>> {
    let (stocks, exp, options, options_quotes) = data;
    let t = teddy::Teddy::new()?;

    // insert equity_tickers
    let mut sq_vec = Vec::<EquityQuote>::new();
    for (_k, v) in stocks {
        for (_, quote) in v {
            sq_vec.push(quote);
        }
    }
    t.insert_eq(sq_vec)?;


    // insert into expirations
    let mut exp_vec = Vec::<Expiration>::new();
    for e in exp {
        let d = Expiration {
            date: e
        };
        exp_vec.push(d);
    }
    t.insert_exp(exp_vec)?;

    // insert options symbols
    let mut os_vec = Vec::<OptionSymbol>::new();
    for (_, s) in options {
        os_vec.push(s);
    };
    t.insert_os(os_vec)?;

    t.insert_oq(options_quotes)?;

    Ok(())
}

/// handle_file will open up the specified zip archive and extract data from the 
/// contained csv files.
fn handle_file(file_name: String, watchlist: &HashSet<String>) -> Result<Data, Box<dyn Error>> {
    let mut stocks = StockMap::new();
    let mut expirations = HashSet::<NaiveDate>::new();
    let mut options = OptionSymbolMap::new();
    let mut option_quotes = Vec::<OptionQuote>::new();

    let r = match File::open(file_name) {
        Ok(f) => f,
        Err(e) => return Err(Box::new(e))
    };
    let mut za = match ZipArchive::new(r) {
        Ok(r) => r,
        Err(e) => return Err(Box::new(e))
    };

    for i in 0..za.len() {
        match za.by_index(i) {
            Ok(f) => {
                let file_name = String::from(f.name());
                let mut rdr = csv::Reader::from_reader(f);
            
                if file_name.contains("stockquotes") {
                    let de = rdr.deserialize::<StockRecord>();
                    for record in de {
                        match record {
                            Ok(r) => handle_stock_record(r, &mut stocks, &watchlist),
                            Err(e) => return Err(Box::new(e))
                        }
                        
                    }
                } else if file_name.contains("optionstats") {
                    for record in  rdr.deserialize::<OptionStatsRecord>() {
                        match record {
                            Ok(r) => handle_option_stats(r, &mut stocks, &watchlist),
                            Err(e) => return Err(Box::new(e))
                        };
                    }
                } else if file_name.contains("options") {
                    for record in rdr.deserialize::<OptionRecord>() {
                        match record {
                            Ok(r) => handle_options(r, &mut options, &mut expirations, &watchlist, &mut option_quotes),
                            Err(e) => return Err(Box::new(e))
                        };
                    }
                } 
        },
            Err(e) => return Err(Box::new(e))
        };
    }
    return Ok((stocks, expirations, options, option_quotes))
}

fn read_watchlist(file_name: &str) -> Result<HashSet::<String>, Box<dyn Error>> {
    let mut w = HashSet::<String>::new();

    let file = File::open(file_name)?;
    let mut rdr = csv::Reader::from_reader(file);

    for r in rdr.deserialize::<WatchlistRecord>() {
        match r {
            Ok(s) => {
                let _ = w.insert(s.ticker);
            },
            Err(e) => return Err(Box::new(e))
        }
    }

    Ok(w)
}

fn insert_watchlist(watchlist: HashSet<String>) -> Result<(), Box<dyn Error>> {
    let t = teddy::Teddy::new()?;

    let mut v = Vec::<EquityTicker>::new();
    for i in watchlist {
        let t = EquityTicker {
            ticker: i.clone()
        };
        v.push(t);
    }
    t.insert_et(v)?;

    Ok(())
}


fn main () {
    println!("Reading Watchlist");
    let watchlist = read_watchlist("docker/data/watchlist.txt").unwrap();

    println!("Finding Zip Archives");
    let glob_files = glob("docker/data/deltaneutral/*.zip").unwrap();
    let mut files = Vec::<String>::new();

    for f in glob_files {
        if let Ok(path) = f {
            files.push(format!("{}", path.display()))
        }
    }
    
    println!("Reading Data");
    let mut i_luv_pbr = ProgressBar::new(files.len() as u64);

    let n_workers = 10;
    let pool = ThreadPool::new(n_workers);

    let (txo, rxo) = channel();
    
    for file in files.clone() {
        let tx = txo.clone();
        let w = watchlist.clone();
        pool.execute(move || {
            match handle_file(file.to_string(), &w) {
                Ok(d) => tx.send(d).unwrap(),
                Err(e) => panic!("{:?}", e),
            }
        });
    }

    // insert the watchlist
    insert_watchlist(watchlist.clone()).unwrap();
    
    // set up structures to track duplicates
    let mut exp = HashSet::<NaiveDate>::new();
    let mut option_symbols = HashSet::<String>::new();

    // insert the watchlist to the database
    for _ in 0..files.len() {
        let (s, e, os, oq) = rxo.recv().unwrap();
        let mut new_os = HashMap::new();
        let mut new_exp = HashSet::new();
        
        // create list of new expirations, add old ones to current list
        for d in e {
            match exp.get(&d) {
                Some(_) => {},
                None => {
                    new_exp.insert(d);
                    exp.insert(d);
                }
            }
        }

        // remove duplicate option symbols, add back remaining ones
        for (k, v) in os {
            match option_symbols.get(&k) {
                Some(_) => {},
                None => {
                    new_os.insert(k.clone(), v);
                    option_symbols.insert(k);
                }
            }
        }


        insert_data((s, new_exp, new_os, oq)).unwrap();

        i_luv_pbr.inc();
    }

    i_luv_pbr.finish();
}
