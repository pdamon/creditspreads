use serde::Deserialize;
use chrono::NaiveDate;

mod date_deserializer {
    use chrono::{NaiveDate};
    use serde::{de::Error, Deserialize, Deserializer};

    pub fn deserialize<'de, D: Deserializer<'de>>(deserializer: D) -> Result<NaiveDate, D::Error> {
        let time: String = Deserialize::deserialize(deserializer)?;
        Ok(NaiveDate::parse_from_str(&time, "%-m/%-d/%Y").map_err(D::Error::custom)?)
    }
}

/// OptionRecord represents a single record within an options file
/// from a Delta Neutral download
#[derive(Debug, Clone, Deserialize)]
pub struct OptionRecord {
    #[serde(rename = "UnderlyingSymbol")]
    pub underlying_symbol: String,
    #[serde(rename = "UnderlyingPrice")]
    pub underlying_price: f64,
    #[serde(rename = "Exchange")]
    pub exchange: String,
    #[serde(rename = "OptionSymbol")]
    pub occ_symbol: String,
    #[serde(rename = "OptionExt")]
    pub option_ext: String,
    #[serde(rename = "Type")]
    pub side: String,
    #[serde(rename = "Expiration")]
    #[serde(with = "date_deserializer")]
    pub expiration: NaiveDate,
    #[serde(rename = "DataDate")]
    #[serde(with = "date_deserializer")]
    pub quotedate: NaiveDate,
    #[serde(rename = "Strike")]
    pub strike: f64,
    #[serde(rename = "Last")]
    pub last: f64,
    #[serde(rename = "Bid")]
    pub bid: f64,
    #[serde(rename = "Ask")]
    pub ask: f64,
    #[serde(rename = "Volume")]
    pub volume: i64,
    #[serde(rename = "OpenInterest")]
    pub open_interest: i64,
    #[serde(rename = "IV")]
    pub iv: f32,
    #[serde(rename = "Delta")]
    pub delta: f32,
    #[serde(rename = "Gamma")]
    pub gamma: f32,
    #[serde(rename = "Theta")]
    pub theta: f32,
    #[serde(rename = "Vega")]
    pub vega: f32,
    #[serde(rename = "AKA")]
    pub aka: String
}

/// OptionStatsRecord represents a single record within an
/// options stats file from a Delta Neutral download
#[derive(Debug, Clone, Deserialize)]
pub struct OptionStatsRecord {
    pub symbol: String,
    #[serde(with = "date_deserializer")]
    pub quotedate: NaiveDate,
    pub iv30call: f32,
    pub iv30put: f32,
    pub iv30mean: f32,
    pub callvol: i64,
    pub putvol: i64,
    pub totalvol: i64,
    pub calloi: i64,
    pub putoi: i64,
    pub comment: String
}

/// StockRecord represents a single record within an
/// stock quotes file from a Delta Neutral download
#[derive(Debug, Clone, Deserialize)]
pub struct StockRecord {
    pub symbol: String,
    #[serde(with = "date_deserializer")]
    pub quotedate: NaiveDate,
    pub open: f64,
    pub high: f64,
    pub low: f64,
    pub close: f64,
    pub volume: i64,
    pub adjustedclose: f64
}

#[derive(Debug, Clone, Deserialize)]
pub struct WatchlistRecord {
    pub ticker: String
}