# Teddy

I'm naming my trading tools after Westworld characters. Teddy is the database portion of the project. The schema is outlined below. Teddy includes a struct called `Teddy` with interface `ChadTeddy` that allow for running database operations.

## TODO

* Add concurrent connections with `r2d2`

## Usage

A podman container with the database can be brought up with `make podman-start`. See Makefile for me details.

There are also docker build file in the docker folder.

A standard call to `Teddy::new` will read the database URL from the variable `DATABASE_URL` in the .env file.
A connection to a database with a custom URL can be specified with `Teddy::new_url`.

### Database creation

Teddy uses Diesel for database interactions. The database can be created/reset using diesel commands. Use `diesel migration redo` to set up the database, or lookup other diesel commands on their website.

### Tables

#### equity_tickers

* ticker VARCHAR(6) *[PK]*

#### expirations

* date DATE PRIMARY KEY *[PK]*

#### option_symbols

* occ_symbol VARCHAR(21) *[PK]*
* underlying VARCHAR(6) *[FK]* equity_tickers(ticker)
* expiration DATE *[FK]* expirations(date)
* strike REAL
* is_call BOOLEAN

#### equity_eod

* ticker VARCHAR(6) *[PK]* *[FK]* equity_tickers(ticker)
* date DATE *[PK]*
* adj_close REAL
* close REAL
* volume BIGINT

#### options_eod

* occ_symbol VARCHAR(21) *[PK]* *[FK]* option_symbols(occ_symbol)
* date DATE *[PK]*
* bid REAL
* ask REAL
* last REAL
* volume BIGINT
* open_interest BIGINT
* iv REAL
* delta REAL
* gamma REAL
* theta REAL
* vega REAL

## License

Copyright Parker Damon 2020 All Rights Reserved
