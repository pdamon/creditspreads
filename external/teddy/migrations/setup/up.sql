-- CREATE SCHEMA public;

-- GRANT ALL ON SCHEMA public TO postgres;
-- GRANT ALL ON SCHEMA public to public;

CREATE TABLE equity_tickers (
    ticker VARCHAR(6) NOT NULL,
    PRIMARY KEY(ticker)
);

CREATE TABLE expirations (
    date DATE NOT NULL,
    PRIMARY KEY (date)
);

CREATE TABLE option_symbols (
    occ_symbol VARCHAR(21) NOT NULL,
    underlying VARCHAR(6) NOT NULL,
    expiration DATE NOT NULL,
    strike DOUBLE PRECISION NOT NULL,
    is_call BOOLEAN NOT NULL,
    PRIMARY KEY(occ_symbol),
    FOREIGN KEY (underlying)
        REFERENCES equity_tickers(ticker),
    FOREIGN KEY (expiration)
        REFERENCES expirations(date)
);

CREATE TABLE equity_eod (
    ticker VARCHAR(6) NOT NULL,
    date DATE NOT NULL,
    adj_close DOUBLE PRECISION,
    -- open DOUBLE PRECISION,
    -- high DOUBLE PRECISION,
    -- low DOUBLE PRECISION,
    close DOUBLE PRECISION,
    volume BIGINT,
    iv30call REAL,
    iv30put REAL,
    iv30mean REAL,
    PRIMARY KEY(ticker, date),
    FOREIGN KEY (ticker)
        REFERENCES equity_tickers(ticker)
);

CREATE TABLE option_eod (
    occ_symbol VARCHAR(21) NOT NULL,
    date date NOT NULL,
    bid DOUBLE PRECISION,
    ask DOUBLE PRECISION,
    last DOUBLE PRECISION,
    volume BIGINT,
    open_interest BIGINT,
    iv REAL,
    delta REAL,
    gamma REAL,
    theta REAL,
    vega REAL,
    PRIMARY KEY(occ_symbol, date),
    FOREIGN KEY (occ_symbol)
        REFERENCES option_symbols(occ_symbol)
);