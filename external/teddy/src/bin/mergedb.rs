use diesel::prelude::*;
use dotenv::dotenv;
use std::env;
use teddy::{get_data::*, get_table_stats, TableStats, Teddy};

fn main() {
    dotenv().ok();

    let src_url = env::var("SRC_URL").unwrap();
    let dest_url = env::var("DEST_URL").unwrap();
    let src_con = PgConnection::establish(&src_url).unwrap();
    let dest_teddy = Teddy::new_url(&dest_url).unwrap();

    println!("Starting SRC Stats");
    let src_start_stats = get_table_stats(&src_con);
    println!("{:?}", src_start_stats);

    println!("Starting DEST Stats");
    let dest_start_stats = dest_teddy.get_table_stats();

    println!("{:?}", dest_start_stats);
    // load the tickers
    let tickers = get_tickers(&src_con).unwrap();
    dest_teddy.insert_et(tickers).unwrap();

    // load the expirations
    let exp = get_exprations(&src_con).unwrap();
    dest_teddy.insert_exp(exp).unwrap();

    // load the option symbols
    let os = get_option_symbols(&src_con).unwrap();
    dest_teddy.insert_os(os).unwrap();

    // load the equity eod quotes
    let eq = get_equity_eod(&src_con).unwrap();
    dest_teddy.insert_eq(eq).unwrap();

    // load the option eod quotes
    let oq = get_options_eod(&src_con).unwrap();
    dest_teddy.insert_oq(oq).unwrap();

    let end_dest_stats = dest_teddy.get_table_stats();
    let dest_diff = TableStats {
        et_size: end_dest_stats.et_size - dest_start_stats.et_size,
        exp_size: end_dest_stats.exp_size - dest_start_stats.exp_size,
        os_size: end_dest_stats.os_size - dest_start_stats.os_size,
        eq_size: end_dest_stats.eq_size - dest_start_stats.eq_size,
        oq_size: end_dest_stats.oq_size - dest_start_stats.oq_size,
    };

    println!("Dest Final Stats");
    println!("{:?}", end_dest_stats);

    println!("Dest Stats Diff");
    println!("{:?}", dest_diff);
}