use std::error::Error;
use std::{env, fs::File, collections::HashSet};

use diesel::{pg::PgConnection, prelude::*};
use dotenv::dotenv;
use serde::Deserialize;

pub mod models;
pub mod get_data;
mod schema;

// this is rust 2015
// but figuring out the new way is hard
// also new way doesn't work with Diesel example projects either
#[macro_use]
extern crate diesel;

// use diesel::prelude::*;

/// A Struct that hold a database connection and can be used to insert and 
/// query data from the database.
pub struct Teddy {
    con: PgConnection,  // a connection to a PG database
    chunk_size: usize,  // the size of chunks in which vectors are written
}

impl Teddy {
    /// Returns as result that can contain a new Teddy struct. If there is a problem with
    /// the setup of .env or an error connecting to the database then an error will
    /// be returned instead.
    pub fn new() -> Result<Teddy, Box<dyn Error>> {
        dotenv()?;
        let url = env::var("DATABASE_URL")?;
        let c = PgConnection::establish(&url)?;
        Ok(Teddy {
            con: c,
            chunk_size: 4096
        })
    }

    /// Returns as result that can contain a new Teddy struct. 
    /// Takes a connection URL as an argument instead of using the
    /// .env file. If there is a problem connecting to the database 
    /// then an error will be returned instead.
    pub fn new_url(url: &str) -> Result<Teddy, Box<dyn Error>> {
        let c = PgConnection::establish(&url)?;
        Ok(Teddy {
            con: c,
            chunk_size: 4096
        })
    }

    /// Inserts the contents of a Vector of type EquityTicker into the database. Values are
    /// inserted in chunks determined by Teddy.chunk_size. `ON CONFLICT DO NOTHING` is set 
    /// so if duplicates detected they will be ignored while all other data will be kept. 
    /// Returns either the number of rows update or an Error.
    pub fn insert_et(&self, d: Vec<models::EquityTicker>) -> Result<usize, Box<dyn Error>> {
        let mut r = 0;
        for chunk in d.chunks(self.chunk_size) {
            match diesel::insert_into(schema::equity_tickers::table)
                .values(chunk)
                .on_conflict_do_nothing()
                .execute(&self.con) {
                    Ok(i) => r += i,
                    Err(e) => return Err(Box::new(e))
            }
        }
        Ok(r)
    }

    /// Inserts the contents of a Vector of type EquityQuote into the database. Values are
    /// inserted in chunks determined by Teddy.chunk_size. `ON CONFLICT DO NOTHING` is set 
    /// so if duplicates detected they will be ignored while all other data will be kept. 
    /// Returns either the number of rows update or an Error.
    pub fn insert_eq(&self, d: Vec<models::EquityQuote>) -> Result<usize, Box<dyn Error>> {
        let mut r = 0;
        for chunk in d.chunks(self.chunk_size) {
            match diesel::insert_into(schema::equity_eod::table)
                .values(chunk)
                .on_conflict_do_nothing()
                .execute(&self.con) {
                    Ok(i) => r += i,
                    Err(e) => return Err(Box::new(e))
            }
        }
        Ok(r)
    }

    /// Inserts the contents of a Vector of type Expiration into the database. Values are
    /// inserted in chunks determined by Teddy.chunk_size. `ON CONFLICT DO NOTHING` is set 
    /// so if duplicates detected they will be ignored while all other data will be kept. 
    /// Returns either the number of rows update or an Error.
    pub fn insert_exp(&self, d: Vec<models::Expiration>) -> Result<usize, Box<dyn Error>> {
        let mut r = 0;
        for chunk in d.chunks(self.chunk_size) {
            match diesel::insert_into(schema::expirations::table)
                .values(chunk)
                .on_conflict_do_nothing()
                .execute(&self.con) {
                    Ok(i) => r += i,
                    Err(e) => return Err(Box::new(e))
            }
        }
        Ok(r)
    }

    /// Inserts the contents of a Vector of type OptionSymbol into the database. Values are
    /// inserted in chunks determined by Teddy.chunk_size. `ON CONFLICT DO NOTHING` is set 
    /// so if duplicates detected they will be ignored while all other data will be kept. 
    /// Returns either the number of rows update or an Error.
    pub fn insert_os(&self, d: Vec<models::OptionSymbol>) -> Result<usize, Box<dyn Error>> {
        let mut r = 0;
        for chunk in d.chunks(self.chunk_size) {
            match diesel::insert_into(schema::option_symbols::table)
                .values(chunk)
                .on_conflict_do_nothing()
                .execute(&self.con) {
                    Ok(i) => r += i,
                    Err(e) => return Err(Box::new(e))
            }
        }
        Ok(r)
    }

    /// Inserts the contents of a Vector of type OptionQuote into the database. Values are
    /// inserted in chunks determined by Teddy.chunk_size. `ON CONFLICT DO NOTHING` is set 
    /// so if duplicates detected they will be ignored while all other data will be kept. 
    /// Returns either the number of rows update or an Error.
    pub fn insert_oq(&self, d: Vec<models::OptionQuote>) -> Result<usize, Box<dyn Error>> {
        let mut r = 0;
        for chunk in d.chunks(self.chunk_size) {
            match diesel::insert_into(schema::option_eod::table)
                .values(chunk)
                .on_conflict_do_nothing()
                .execute(&self.con) {
                    Ok(i) => r += i,
                    Err(e) => return Err(Box::new(e))
            }
        }
        Ok(r)
    }

    // Executes SQL query q against the given database connection and returns the results
    pub fn sql_query<U>(&self, q: String) -> QueryResult<Vec<U>> 
    where
        U: diesel::deserialize::QueryableByName<diesel::pg::Pg>
    {
        diesel::sql_query(q)
            .load(&self.con)
    }

    // Gets that table statistics for current connection.
    pub fn get_table_stats(&self) -> TableStats {
        get_table_stats(&self.con)
    }

    // Selects options quotes from the database.
    // Filters on the value of ticker and whether or not it is a call or put option.
    pub fn get_option_quotes(&self, ticker: String, is_call: bool) -> QueryResult<Vec::<models::FullOptionQuote>> {
        schema::option_eod::table.left_join(schema::option_symbols::table)
            .left_join(
                schema::equity_eod::table.on(
                    schema::equity_eod::ticker.eq(schema::option_symbols::underlying)
                        .and(schema::equity_eod::date.eq(schema::option_eod::date)
                    )
                )
            )
            .filter(schema::option_symbols::underlying.eq(ticker))
            .filter(schema::option_symbols::is_call.eq(is_call))
            .load::<models::FullOptionQuote>(&self.con)
    }

    // Selects equity quotes from the database.
    // Filters on the value of ticker
    pub fn get_equity_quotes(&self, ticker: String) -> QueryResult::<Vec::<models::EquityQuote>> {
        schema::equity_eod::table
            .filter(schema::equity_eod::ticker.eq(ticker))
            .load(&self.con)
    }
}


/// These tests need to be run on a clean database. 
/// Don't use the production one that already has a ton of data
/// in it.
#[cfg(test)]
mod test {
    use super::*;

    /// Test to determine the functionality of on_conflict_do_nothing. Shows that 
    /// non-duplicate rows are still inserted, while the duplicated are ignored.
    #[test]
    fn test_duplicate_write() {
        let t1 = models::EquityTicker {
            ticker: "SPY".to_string()
        };

        let t2 = models::EquityTicker {
            ticker: "IWM".to_string()
        };

        let t3 = models::EquityTicker {
            ticker: "QQQ".to_string()
        };

        let v1 = vec![t1, t2.clone()];
        let v2 = vec![t2, t3];

        let teddy = Teddy::new().unwrap();

        let r1 = teddy.insert_et(v1);
        let r2 = teddy.insert_et(v2);

        assert_eq!(2, r1.unwrap());
        assert_eq!(1, r2.unwrap());
    }
}

/// Watchlist represent a single ticker that can be read in 
/// from CSV and deserialized with serde.
#[derive(Debug, Clone, Deserialize)]
struct WatchlistRecord {
    pub ticker: String
}

/// Reads in a watchlist from a csv file. Returns the watchlist as a HashSet
/// or an error if encountered.
pub fn read_watchlist(file_name: &str) -> Result<HashSet::<String>, Box<dyn Error>> {
    let mut w = HashSet::<String>::new();

    // open csv file
    let file = File::open(file_name)?;
    let mut rdr = csv::Reader::from_reader(file);


    // read in the tickers
    for r in rdr.deserialize::<WatchlistRecord>() {
        match r {
            Ok(s) => {
                let _ = w.insert(s.ticker);
            },
            Err(e) => return Err(Box::new(e))
        }
    }

    Ok(w)
}

pub fn get_table_stats(con: &PgConnection) -> TableStats {
    let et_size: i64 = schema::equity_tickers::table.count().get_result(con).unwrap();
    let os_size: i64 = schema::option_symbols::table.count().get_result(con).unwrap();
    let exp_size: i64 = schema::expirations::table.count().get_result(con).unwrap();
    let eq_size: i64 = schema::equity_eod::table.count().get_result(con).unwrap();
    let oq_size: i64 = schema::option_eod::table.count().get_result(con).unwrap();

    return TableStats {
        et_size: et_size,
        os_size: os_size,
        exp_size: exp_size,
        eq_size: eq_size,
        oq_size: oq_size
    }
}

#[derive(Debug)]
pub struct TableStats {
    pub et_size: i64,
    pub os_size: i64,
    pub exp_size: i64,
    pub eq_size: i64,
    pub oq_size: i64,
}