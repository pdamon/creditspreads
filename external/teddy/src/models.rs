use super::schema::*;

use chrono::NaiveDate;

/// Holds information on a quote for an equity (Stock/ETF)
/// that can be queried from and inserted to the database.
#[derive(Debug, Insertable, Queryable)]
#[table_name = "equity_eod"]
pub struct EquityQuote {
    pub ticker: String,
    pub date: NaiveDate,
    pub adj_close: Option<f64>,
    pub close: Option<f64>,
    pub volume: Option<i64>,
    pub iv30call: Option<f32>,
    pub iv30put: Option<f32>,
    pub iv30mean: Option<f32>,
}

/// Holds information on a quote for a options symbols
/// that can be queried from and inserted into the database.
#[derive(Debug, Insertable, Queryable)]
#[table_name = "option_eod"]
pub struct OptionQuote {
    pub occ_symbol: String,
    pub date: NaiveDate,
    pub bid: Option<f64>,
    pub ask: Option<f64>,
    pub last: Option<f64>,
    pub volume: Option<i64>,
    pub open_interest: Option<i64>,
    pub iv: Option<f32>,
    pub delta: Option<f32>,
    pub gamma: Option<f32>,
    pub theta: Option<f32>,
    pub vega: Option<f32>,
}

/// Holds information on a option symbols. This provides a mapping
/// from OCC style symbols to data points of what they represent.
#[derive(Debug, Insertable, Queryable)]
#[table_name = "option_symbols"]
pub struct OptionSymbol {
    pub occ_symbol: String,
    pub underlying: String,
    pub expiration: NaiveDate,
    pub strike: f64,
    pub is_call: bool
}


/// Holds the name of a equity (Stock/ETF) ticker that can be 
/// queried from and inserted into the database.
#[derive(Clone, Debug, Insertable, Queryable)]
#[table_name = "equity_tickers"]
pub struct EquityTicker {
    pub ticker: String
}

/// Holds the data of an option expiration data, which can be queried from
/// and inserted into the database.
#[derive(Debug, Insertable, Queryable)]
#[table_name = "expirations"]
pub struct Expiration {
    pub date: NaiveDate
}


// Contains all possible data for an option quote. 
// This includes the option quote, all OCC symbol information
// and a full quote for the underlying.
#[derive(Debug, Queryable)]
pub struct FullOptionQuote {
    pub quote: OptionQuote,
    pub symbol_data: Option<OptionSymbol>,
    pub underlying_data: Option<EquityQuote>,
}