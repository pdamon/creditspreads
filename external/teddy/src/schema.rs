table! {
    equity_eod (ticker, date) {
        ticker -> Varchar,
        date -> Date,
        adj_close -> Nullable<Float8>,
        // open -> Nullable<Float8>,
        // high -> Nullable<Float8>,
        // low -> Nullable<Float8>,
        close -> Nullable<Float8>,
        volume -> Nullable<Int8>,
        iv30call -> Nullable<Float4>,
        iv30put -> Nullable<Float4>,
        iv30mean -> Nullable<Float4>,
    }
}

table! {
    equity_tickers (ticker) {
        ticker -> Varchar,
    }
}

table! {
    expirations (date) {
        date -> Date,
    }
}

table! {
    option_eod (occ_symbol, date) {
        occ_symbol -> Varchar,
        date -> Date,
        bid -> Nullable<Float8>,
        ask -> Nullable<Float8>,
        last -> Nullable<Float8>,
        volume -> Nullable<Int8>,
        open_interest -> Nullable<Int8>,
        iv -> Nullable<Float4>,
        delta -> Nullable<Float4>,
        gamma -> Nullable<Float4>,
        theta -> Nullable<Float4>,
        vega -> Nullable<Float4>,
    }
}

table! {
    option_symbols (occ_symbol) {
        occ_symbol -> Varchar,
        underlying -> Varchar,
        expiration -> Date,
        strike -> Float8,
        is_call -> Bool,
    }
}

joinable!(equity_eod -> equity_tickers (ticker));
joinable!(option_eod -> option_symbols (occ_symbol));
joinable!(option_symbols -> equity_tickers (underlying));
joinable!(option_symbols -> expirations (expiration));

allow_tables_to_appear_in_same_query!(
    equity_eod,
    equity_tickers,
    expirations,
    option_eod,
    option_symbols,
);
