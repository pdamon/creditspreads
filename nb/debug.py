from utils import *

def main():
    spread_width = 5
    ticker = "GLD"
    path = "output/gld_vp5.csv"
    # min_dte = 30
    # max_dte = 60
    # min_delta = 0.05
    # max_delta = 0.6
    # min_volume = 100
    spread_type = "vertical_put"
    # profit_target = 0.25
    # sdd = 0.03
    cradle(path, ticker, spread_width, spread_type, 80)

if __name__ == "__main__":
    main()