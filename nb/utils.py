import pandas as pd
from matplotlib import pyplot as plt
from sqlalchemy import create_engine
from subprocess import Popen, PIPE
from os import environ
from queue import Queue, Empty
from threading import Thread

con = create_engine("postgres+psycopg2://postgres:test@localhost:5432/postgres")

def get_data(path, ticker, spread_width):
    r = pd.read_csv(path)

    iv_q = """
        SELECT date,
            iv30mean
        FROM equity_eod
        WHERE ticker = '{}'
    """.format(ticker)
    iv = pd.read_sql(iv_q, con)
    calcivp = lambda x: pd.Series(x).rank(pct=True).iloc[-1]
    iv['date'] = pd.to_datetime(iv['date'])
    iv['ivp60'] = iv['iv30mean'].rolling(60).apply(calcivp)
    iv['ivp365'] = iv['iv30mean'].rolling(365).apply(calcivp)

    # create new columns
    r["open_date"] = pd.to_datetime(r["open_date"])
    r["close_date"] = pd.to_datetime(r["close_date"])
    r['days_held'] = (r["close_date"] - r["open_date"]).dt.days
    r['profit'] = r['open_credit'] - r['close_debit']
    r["roi"] = r['profit']/(spread_width - r['open_credit'])
    r['daily_roi'] = (1 + r['roi'])**(1/r['days_held']) - 1
    r['annualized_roi'] = (1 + r['daily_roi'])**365
    r['seller_win'] = r['profit'].apply(lambda x: 1 if x > 0 else 0)

    # filter to make sure data is valid
    r = r[(r['max_debit_close'] > 0) & (r['max_debit_close'] < spread_width)]
    r = r[(r['profit'] > -spread_width) & (r['profit'] < spread_width)]
    r = r[(r['open_credit'] > 0) & (r['open_credit'] < spread_width)]
    r = r[r['days_held'] > 0]

    df = r.merge(iv, left_on=["open_date"], right_on=['date']).dropna().drop_duplicates()

    return df

def bear_bull_split(df):
    try:
        gbm = df[(df['open_date'] > '2007-09-01') & (df['open_date'] < '2009-03-09')]
        sbm = df[(df['open_date'] > '2009-03-09')]
    except ValueError:
        return None, None

    if len(gbm) == 0 or len(sbm) == 0:
        return None, None
    
    return gbm, sbm

def get_high_ivp60(df, spread_width, thresh):
    return df[(df['ivp60'] >= thresh)]

def get_high_ivp365(df, spread_width, thresh):
    return df[(df['ivp365'] >= thresh)]

def _nq_output(out, q):
    for line in iter(out.readline, b''):
        q.put(line.strip().decode())
    out.close()

def run_backtest(path, ticker, spread_width, spread_type, profit_target, sdd, min_dte, max_dte, min_delta, max_delta, min_volume):
    url = "postgres://postgres:test@localhost:5432/postgres"
    env = environ.copy()
    env["DATABASE_URL"] = "{}".format(url)
    env["OUTPUT_PATH"] = "{}".format(path)
    env["LOW_DELTA"] = "{}".format(min_delta)
    env["HIGH_DELTA"] = "{}".format(max_delta)
    env["UNDERLYING"] = "{}".format(ticker)
    env["MIN_VOLUME"] = "{}".format(min_volume)
    env["SPREAD_WIDTH"] = "{}".format(spread_width)
    env["MIN_DTE"] = "{}".format(min_dte)
    env["MAX_DTE"] = "{}".format(max_dte)
    env["SPREAD_TYPE"] = "{}".format(spread_type)
    env["PROFIT_TARGET"] = "{}".format(profit_target)
    env["SHORT_DELTA_DIFF"] = "{}".format(sdd)    
    cmd = "../target/release/cradle"

    process = Popen(cmd, stdout=PIPE, stderr=PIPE, env=env)
    q = Queue()
    t_o = Thread(target=_nq_output, args=(process.stdout, q))
    t_e = Thread(target=_nq_output, args=(process.stderr, q))

    t_o.daemon = True
    t_o.start()
    t_e.daemon = True
    t_e.start()
    while True:
        if process.poll() is not None:
            break
        try: line = q.get_nowait()
        except Empty:
            pass
        else:
            print(line)

    if process.poll() == 0:
        print("Backtest completed successfully")
    else:
        print("Backtest encountered errors")

def plot_short_vertical_data(axes, df, l):
    # df_ivp60_cut = df.groupby(pd.cut(df['ivp60'], 20)).mean()
    df_ivp365_cut = df.groupby(pd.cut(df['ivp365'], 20)).mean()

    # ivp vs performace plot
    # axes[0].plot(df_ivp60_cut['ivp60'], df_ivp60_cut['profit'], "o-", label="{} IVP60 Mean".format(l))
    axes[0].plot(df_ivp365_cut['ivp365'], df_ivp365_cut['profit'], "o-", label="{} IVP365 Mean".format(l))
    axes[0].axhline(0)
    axes[0].legend()
    axes[0].set_xlabel('IV Percentile')
    axes[0].set_ylabel('Average Profit')
    axes[0].set_title("IVP vs Profit")
    
    # delta vs seller wins plot
    days = df.groupby(pd.cut(df['short_delta'], 20)).mean()
    axes[1].plot(days['short_delta'], days['seller_win'], label="{}".format(l))
    axes[1].axhline(0.9)
    axes[1].axhline(1)
    axes[1].legend()
    axes[1].set_xlabel('Short Leg Delta')
    axes[1].set_ylabel('Seller Win')
    axes[1].set_title("Seller Win vs Short Leg Delta")

    # delta vs roi
    # axes[2].plot(days['short_delta'], days['roi'], label="{} %ROI".format(l))
    axes[2].plot(days['short_delta'], 100*days['daily_roi'], label="{} %Daily ROI".format(l))
    axes[2].legend()
    axes[2].set_xlabel('Short Leg Delta')
    axes[2].set_ylabel('%Daily ROI')
    axes[2].set_title("ROI vs Short Leg Delta")
    
    # holding times hist
    axes[3].hist(df['days_held'], density=True, cumulative=True, histtype='step', label="{}".format(l))
    axes[3].legend(loc='lower center')
    axes[3].set_xlabel('Days Held')
    axes[3].set_ylabel('Frequency')
    axes[3].set_title("Overall Holding Times")
    
    # holding times by delta
    axes[4].plot(days['short_delta'], days['days_held'], label="{}".format(l))
    axes[4].axhline(0)
    axes[4].legend(loc='lower center')
    axes[4].set_xlabel('Short Leg Delta')
    axes[4].set_ylabel('Mean Holding Time')
    axes[4].set_title("Holding Time vs Short Leg Delta")

def plot_iron_condor_data(axes, df, l):
    # df_ivp60_cut = df.groupby(pd.cut(df['ivp60'], 20)).mean()
    df_ivp365_cut = df.groupby(pd.cut(df['ivp365'], 20)).mean()

    # ivp vs performace plot
    # axes[0].plot(df_ivp60_cut['ivp60'], df_ivp60_cut['profit'], "o-", label="{} IVP60 Mean".format(l))
    axes[0].plot(df_ivp365_cut['ivp365'], df_ivp365_cut['profit'], "o-", label="{} IVP365 Mean".format(l))
    axes[0].axhline(0)
    axes[0].legend()
    axes[0].set_xlabel('IV Percentile')
    axes[0].set_ylabel('Average Profit')
    axes[0].set_title("IVP vs Profit")
    
    # delta vs seller wins plot
    days = df.groupby(pd.cut(df['short_put_delta'], 20)).mean()
    axes[1].plot(days['short_put_delta'], days['seller_win'], label="{}".format(l))
    axes[1].axhline(0.9)
    axes[1].axhline(1)
    axes[1].legend()
    axes[1].set_xlabel('Short Leg Delta')
    axes[1].set_ylabel('Seller Win')
    axes[1].set_title("Seller Win vs Short Leg Delta")

    # delta vs roi
    # axes[2].plot(days['short_delta'], days['roi'], label="{} %ROI".format(l))
    axes[2].plot(days['short_put_delta'], 100*days['daily_roi'], label="{} %Daily ROI".format(l))
    axes[2].legend()
    axes[2].set_xlabel('Short Leg Delta')
    axes[2].set_ylabel('%Daily ROI')
    axes[2].set_title("ROI vs Short Leg Delta")
    
    # holding times hist
    axes[3].hist(df['days_held'], density=True, cumulative=True, histtype='step', label="{}".format(l))
    axes[3].legend(loc='lower center')
    axes[3].set_xlabel('Days Held')
    axes[3].set_ylabel('Frequency')
    axes[3].set_title("Overall Holding Times")
    
    # holding times by delta
    axes[4].plot(days['short_put_delta'], days['days_held'], label="{}".format(l))
    axes[4].axhline(0)
    axes[4].legend(loc='lower center')
    axes[4].set_xlabel('Short Leg Delta')
    axes[4].set_ylabel('Mean Holding Time')
    axes[4].set_title("Holding Time vs Short Leg Delta")


def cradle(path, ticker, spread_width, type, high_iv_thresh, bear_bull=False):
    if (type == "iron_condor") or (type == "iron_butterfly") or (type == "icdv") or (type == "ifdv"):
        # get the data
        df = get_data(path, ticker, spread_width)
        
        # create 3xX subplot
        _, ax = plt.subplots(1,5,figsize=(35,7))
        
        # OVERALL
        plot_iron_condor_data(ax, df.copy(), "Overall")
        high_ivp60 = get_high_ivp60(df.copy(), spread_width, high_iv_thresh)
        high_ivp365 = get_high_ivp365(df.copy(), spread_width, high_iv_thresh)

        print(len(high_ivp60))
        plot_iron_condor_data(ax, high_ivp60, "High IVP60")
        plot_iron_condor_data(ax, high_ivp365, "High IVP365")

        if bear_bull:
            gbm, sbm = bear_bull_split(df)
            if gbm is not None and sbm is not None:
                plot_iron_condor_data(ax, gbm, "Bear")
                plot_iron_condor_data(ax, sbm, "Bull")
    else:
        # get the data
        df = get_data(path, ticker, spread_width)
        
        # create 3xX subplot
        _fig, ax = plt.subplots(1,5,figsize=(35,7))
        
        # OVERALL
        plot_short_vertical_data(ax, df.copy(), "Overall")
        high_ivp60 = get_high_ivp60(df.copy(), spread_width, high_iv_thresh)
        high_ivp365 = get_high_ivp365(df.copy(), spread_width, high_iv_thresh)
        plot_short_vertical_data(ax, high_ivp60, "High IVP60")
        plot_short_vertical_data(ax, high_ivp365, "High IVP365")

        if bear_bull:
            gbm, sbm = bear_bull_split(df)
            if gbm is not None and sbm is not None:
                plot_short_vertical_data(ax, gbm, "Bear")
                plot_short_vertical_data(ax, sbm, "Bull")
            