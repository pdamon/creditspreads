use crate::readdata::get_chains;

use armistice::{trade::{ManageResult, Trade, TradeError}, verticalput::VerticalPut};
use bigdecimal::BigDecimal;
use csv::Writer;
use pbr::ProgressBar;

pub fn run_backtest(ticker: &str, w: BigDecimal) {
    println!("Getting option chains");
    let hist_chains = get_chains(ticker).unwrap();
    let mut wtr = Writer::from_path("test.csv").unwrap();
    wtr.write_record(VerticalPut::write_header()).unwrap();

    println!("Starting Backtest");
    let mut positions = Vec::new();
    let mut i_luv_pbr = ProgressBar::new(hist_chains.len() as u64);
    let op = armistice::trade::MakeParams::new(w.clone());

    println!("Simluating {} possible days.", hist_chains.len());
    
    for (d, c) in hist_chains {
        // push new chains to the list of open positions
        for mut s in VerticalPut::make(&c, &op) {
            s.open(0.0, d.clone(), &c).unwrap();
            positions.push(s);
        }

        let mp = armistice::trade::ManageParams::new(0.25, 0.0, d.clone());
        for p in &mut positions {
            match p.manage(&c, &mp) {
                Ok(r) => match r {
                    ManageResult::Nothing => {
                        // do nothing I guess
                    },
                    ManageResult::Close | 
                    ManageResult::Assigned => {
                        wtr.write_record(p.write()).unwrap();
                    }
                },
                Err(e) => {
                    match e {
                        TradeError::ChainMissingExpiration | TradeError::ChainMissingStrike => {},
                        TradeError::SpreadNotOpen | TradeError::SpreadNotClosed => {
                            panic!("Chain: {:?} Current Date: {} {}", p, d, e);
                        },
                    }
                }
            }

            p.update_stats(&c);
        }

        positions.retain(|x| x.is_open());
        i_luv_pbr.inc();
    }

    i_luv_pbr.finish();

    println!("Backtest finished with {} spreads still open.", positions.len());
}