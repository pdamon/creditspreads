mod readdata;
mod backtest;

use bigdecimal::BigDecimal;

fn main() {
    let t = "SPY";

    backtest::run_backtest(t, BigDecimal::from(5));
}