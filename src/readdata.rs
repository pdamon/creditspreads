use std::{error::Error, collections::BTreeMap};

use armistice::option::{Chain, Quote, Side};
use chrono::NaiveDate;
use teddy::Teddy;

// #[derive(Debug, Clone)]
// pub enum SpreadType {
//     VerticalCall,
//     VerticalPut,
//     IronCondor,
//     IronButterfly,
//     IronCondorAsDV,
//     IronButterflyAsDV,
// }


/// Gets all historical option chains from Teddy for the given ticker.
/// 
/// # Example
/// 
/// ```
/// let chain = get_chains("GLD");
/// ```
pub fn get_chains(ticker: &str) -> Result<BTreeMap<NaiveDate, Chain>, Box<dyn Error>> {
    let t = Teddy::new()?;
    let calls = t.get_option_quotes(ticker.to_string(), true)?;
    let puts = t.get_option_quotes(ticker.to_string(), false)?;

    let mut m = BTreeMap::new();
    for c in calls {
        let chain = match m.get_mut(&c.quote.date) {
            Some(oc) => oc,
            None => {
                let nc = Chain::new();
                m.insert(c.quote.date, nc);

                m.get_mut(&c.quote.date).unwrap()
            }
        };

        let q = Quote::new(
            c.quote.bid.ok_or("Bad Bid Price")?,
            c.quote.ask.ok_or("Bad Ask Price")?,
            c.quote.last.ok_or("Bad Last")?,
            c.quote.delta.ok_or("Bad Delta")?,
            c.quote.delta.ok_or("Bad Delta")?,
            c.quote.gamma.ok_or("Bad Gamma")?,
            c.quote.theta.ok_or("Bad Theta")?,
            c.quote.vega.ok_or("Bad Vega")?,
            c.quote.iv.ok_or("Bad IV")?
        );
        
        let sd = c.symbol_data.ok_or("Bad Symbol Data")?;
        // insert to the chain
        chain.insert_quote(q, sd.expiration, sd.strike, Side::Call);
    }

    for p in puts {
        // get the chain to insert to
        let chain = match m.get_mut(&p.quote.date) {
            Some(oc) => oc,
            None => {
                let nc = Chain::new();
                m.insert(p.quote.date, nc);

                m.get_mut(&p.quote.date).unwrap()
            }
        };

        // construct the quote
        let q = Quote::new(
            p.quote.bid.ok_or("Bad Bid Price")?,
            p.quote.ask.ok_or("Bad Ask Price")?,
            p.quote.last.ok_or("Bad Last")?,
            p.quote.delta.ok_or("Bad Delta")?,
            p.quote.delta.ok_or("Bad Delta")?,
            p.quote.gamma.ok_or("Bad Gamma")?,
            p.quote.theta.ok_or("Bad Theta")?,
            p.quote.vega.ok_or("Bad Vega")?,
            p.quote.iv.ok_or("Bad IV")?
        );

        let sd = p.symbol_data.ok_or("Bad Symbol Data")?;
        // insert to the chain
        chain.insert_quote(q, sd.expiration , sd.strike, Side::Put);
    }
    
    Ok(m)
}